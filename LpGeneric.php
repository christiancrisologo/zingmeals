<?php
class LpGeneric
{
	const OBJECT_SESSION_NAME = 'someSession42544';
	
	private $_SessionName = self::OBJECT_SESSION_NAME;
	
	public $caid = 32397;
	public $cid2 = 21409;
	public $caCode = 'DEFAULT';
	public $stid = 465;
	public $misc1 = "";
	public $misc2 = "";
	public $misc3 = "";
	
	public $WasClickPixelFired = false;
	public $WasForcedClickPixelFired = false;
	public $WasOVPixelFired = false;
	
	
	public function ShowPixel($IsStartPage, $ForcedCreativeID = 0)
	{
		$pixelCode = '';
	
		if( ( ($IsStartPage && !$this->WasClickPixelFired) || (!$IsStartPage && !$this->WasOVPixelFired) ) || ($ForcedCreativeID && !$this->WasForcedClickPixelFired) )
		{
			$PixelType = ($IsStartPage ? 2 : 3);
			
			$cid2 = (int)$this->cid2;
			if($ForcedCreativeID > 0) 
			{
				$cid2 = $ForcedCreativeID;
				$PixelType = 2;
			}
			
			if( ((int)$this->caid > 0) && ($cid2 > 0) && (strlen($this->caCode) > 0) )
			{
				$pixelCode = "<!--pixelcode start--><img src='https://secure1.m57media.com/clients/p2c/u/pc/?cid1=0&cid2=".$cid2."&cid3=0&cid4=0&caid=".$this->caid."&caCode=".$this->caCode."&stID=".$this->stid."&eType=".$PixelType."&misc1=".$this->misc1."&misc2=".$this->misc2."&misc3=".$this->misc3."' style='height:1px;width:1px;border-width:0px;position:absolute'/><!--pixelcode end-->";

				if($ForcedCreativeID)
				{
					$this->WasForcedClickPixelFired = true;
				}
				else 
				{				
					if($IsStartPage)
					{
						$this->WasClickPixelFired = true;
					}
					else
					{
						$this->WasOVPixelFired = true;
					}
				}
				
				$this->SaveToSession();
			}
		}
		
		return $pixelCode;
	}
	
	public function SaveToSession() 
	{
		$_SESSION[$this->_SessionName] = serialize($this);
	}

	private static function ParseObject() 
	{
		$newObject = new LpGeneric();
		
		$newObject->stid = (isset($_GET['stid']) ? $_GET['stid'] : $newObject->stid);
		$newObject->caid = (isset($_GET['caid']) ? $_GET['caid'] : $newObject->caid);
		$newObject->cid2 = (isset($_GET['cid2']) ? $_GET['cid2'] : $newObject->cid2);
		$newObject->caCode = (isset($_GET['cacode']) ? $_GET['cacode'] : $newObject->caCode);
		$newObject->caCode = (isset($_GET['caCode']) ? $_GET['caCode'] : $newObject->caCode);

		$newObject->misc1 = (isset($_GET['misc1']) ? $_GET['misc1'] : '');
		$newObject->misc2 = (isset($_GET['misc2']) ? $_GET['misc2'] : '');
		$newObject->misc3 = (isset($_GET['misc3']) ? $_GET['misc3'] : '');
		
		return $newObject;
	}
	
	public static function RestoreObject($SessionName = self::OBJECT_SESSION_NAME)
	{
		$newObject = null;
		//trying to deserialize object from session
		if(isset($_SESSION[$SessionName])) 
		{
			$newObject = self::reCreate(unserialize($_SESSION[$SessionName]));
		}
		//checking that serialization processed successfully
		if(!$newObject)
		{
			//if not, checking URL params
			$newObject = self::ParseObject();
			
			$newObject->_SessionName = $SessionName;
			//save object to session
			$newObject->SaveToSession();
		}
		
		return $newObject;
	}
	
	//fix for deserialization of object
	private static function reCreate($obj) {
        $obj = (array) $obj;
        $newObj = new LpGeneric();
        foreach($obj as $k => $v) {
            $parts = explode(chr(0), $k);
            if ($k != '__PHP_Incomplete_Class_Name') {
                if ($parts[0] == $k) 
                    $newObj->reCreatePrivate($k, $v);
                else 
                    $newObj->reCreatePrivate($parts[2], $v);
            }
        }
        return $newObj;
    }
    
    private function reCreatePrivate($name, $value) {
        $this->$name = $value;
    }
	
	public function PrepareGetParams($newCid = null) 
	{
		return "caid=".$this->caid."&misc1=".$this->misc1."&misc2=".$this->misc2."&misc3=".$this->misc3."&caCode=".$this->caCode."&nopop=0&stid=".$this->stid."&cid2=".($newCid ? $newCid : $this->cid2)."&go=2";
	}
}
?>