<?php include 'components/header.php'; ?>
<?php include 'LpGeneric.php';
	//init object
	$objLpGeneric = LpGeneric::RestoreObject();
?>
<?php echo $objLpGeneric->ShowPixel(false);?>

<section id="order-section"  >
<form id="billing-information" >
    <div class="container"> 
 
	<!-- Start Content -->
	<div class="row content">
        <div class="col-md-6 col-sm-6 col-xs-12">
			<h2 class="align-center blue">Try Healthy-Finds risk-free and receive $85 in immediate savings</h2>
           	<br/> <img class="img-responsive" src="assets/images/order-hf-product-logo.png?1234p" />
			<br/>


					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<label class="radio-opt-payment">
								<input type="radio" name="paymenttermopt" value="0" required checked>
								<span class="txt" ><strong>$4 per month </strong><br/><span class="i">Pay only $4 today. In 30 days, your card will be billed one time for $48 for a full year membership.  It will not be charged again.</span></span>
							</label>
						</div>
					</div>
					<br/>

					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<label class="radio-opt-payment">
								<input type="radio" name="paymenttermopt" value="1" required >
								<span class="txt"><strong> $7 per month  </strong><br/> <span class="i">Pay only $7 today.  In 30 days, your card will be billed $27.95, and every 4 months thereafter.</span></span>
							</label>
						</div>
					</div>
					<br/>

					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<label class="radio-opt-payment">
								<input type="radio" name="paymenttermopt" value="2" required >
								<span class="txt"><strong> $9.95 per month  </strong><br/><span class="i">Sign up today and pay only $9.95.  Your card will be billed $9.95 every 30 days for your convenience.  You can cancel anytime.</span></span>
							</label>
						</div>
					</div>
					<br/>
			<br/>
		 	<p class="paragraph"><strong>If you don’t love it, get your money back and keep your free gifts – no questions asked. If you want to cancel, simply click on a link or send us an email. But who doesn’t want to save tons of money and discover the most cutting-edge health products, right?<strong></p>
			<p class="paragraph blue3"><strong>This Risk-Free offer is only available to the first 100 people to sign up today. You will never get this offer again. Claim it now, you have nothing to lose!</strong></p>
              
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
			<h2   style="width:100% " class="align-center"><span class="gray">Order Now,  you won’t regret it!</span></h2>
			<br/>
			<div class="gray-fade-box">
				
        
					
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<label for="first-name">First Name</label>
							<input type="text" class="form-control input-fields" name="firstname" required/>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<label for="last-name">Last Name</label>
							<input type="text" class="form-control input-fields" name="lastname" required/>
						</div>						
					</div>
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<label for="email">Email</label>
							<input type="email" class="form-control input-fields" name="email" required/>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<label for="phone-number">Phone No</label>
							<input type="tel" class="form-control input-fields" name="phonenumber" required/>
						</div>						
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<label for="address">Address</label>
							<input type="text" class="form-control input-fields" name="address1" required/>
						</div>				
					</div>
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<label for="city">City</label>
							<input type="text" class="form-control input-fields" name="city" required/>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<label for="state">State</label>
							<div class="select-side arrow-right"><i class="fa fa-sort-down fa-fw"></i></div>
							<select id="state-select" class="form-control input-fields" name="state" required>
								<option value="" selected="selected"></option>
								<?php
									require "components/states_list.php";
									foreach($states as $key=>$value):
								?>
								<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<label for="zip-code">Zip Code</label>
							<input type="text" class="form-control input-fields" name="zipcode" required/>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<label for="card-type">Credit Card Type</label>
							<div class="select-side arrow-right"><i class="fa fa-sort-down fa-fw"></i></div>
							<select class="form-control input-fields" name="card-type" required>
								<option value="" selected="selected"></option>
								<option value="MasterCard" label="MasterCard">MasterCard</option>
								<option value="Visa" label="Visa">Visa</option>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<label for="card-number">Card Number</label>
							<input type="text" class="form-control input-fields" name="cardNumber" required/>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<label for="cvc">CVC</label>
							<input type="text" class="form-control input-fields" name="CVV" required/>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-12">
							<label for="expiry">Expiry MM/YYYY</label>
							<table style="width: 100%;">
								<tr>
									<td class="expiration-options" style="padding-right: 1px;">
										<div class="select-side exp"><i class="fa fa-sort-desc fa-fw"></i></div>
										<select class="input-fields form-control" name="expirationMonth" required>
											<option value="" selected="selected"></option>
											<?php for($month=1;$month<=12;$month++): ?>
											<option value="<?php echo $month; ?>"><?php echo $month; ?></option>
											<?php endfor; ?>
										</select>
									</td>
									<td class="expiration-options" style="padding-left: 1px;">
										<div class="select-side exp"><i class="fa fa-sort-desc fa-fw"></i></div>
										<select class="input-fields form-control" name="expirationYear" required>
											<option value="" selected="selected"></option>
											<?php for($year=date('Y');$year<=(date('Y')+5);$year++): ?>
											<option value="<?php echo $year; ?>"><?php echo $year; ?></option>
											<?php endfor; ?>
										</select>
									</td>
								</tr>
							</table>
						</div>
						<div class="col-md-4 col-sm-4 hidden-xs">&nbsp;</div>
						<div class="col-md-4 col-sm-4 hidden-xs">&nbsp;</div>
					</div>
					<br />
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<p id="i-agree-container" class="i-agree">
								<input type="checkbox" id="yes-i-want" class="css-checkbox" name="updates" value="1" checked/>
								<label for="yes-i-want" name="checkbox2_lbl" class="css-label lite-orange-check">I want to receive updates and special offers from <?php echo $CONFIG_UPSELL_PRODUCT; ?>.</label>
							</p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12 align-center">
							<button class="button align-center" style="padding: 20px 50px 15px 50px !important;">BUY NOW!</button>
						</div>
					</div>
					<br />
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<p id="terms-checkbox" class="i-agree">
								<label for="yes-i-agree" name="checkbox2_lbl">By clicking Buy Now you are agreeing to the <a href="terms.php" target="_blank">Terms and Conditions</a> and <a href="policy.php" target="_blank">Privacy Policy.</a></label>
								<script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async></script>
							</p>
						</div>
					</div>
				
					<br />
					<div id="billing-form-response"></div>
					<div class="row">
					
					<?php include 'components/seals.php'; ?>
					</div>
			</div>
				
			<br />
		
		</div>
	</div>
</div> 
</form>
</section>

<?php include 'components/footer.php'; ?>