<?php
session_start();
require_once(__DIR__ .  '/../config.php'); 
include '../LpGeneric.php';


//init object
$objLpGeneric = LpGeneric::RestoreObject();

include_once('../nusoap.php');

function checkRequiredFields() {
	$error = null;
	// if(empty($_POST['paymenttermopt'])) {$error[] = 'Payment term option is required';}
	if(empty($_POST['firstname'])) {$error[] = 'First name is required';}
	if(empty($_POST['lastname'])) {$error[] = 'Last name is required';}
	if(empty($_POST['address1'])) {$error[] = 'Address is required';}
	if(empty($_POST['city'])) {$error[] = 'City is required';}
	if($_POST['shortstate'] == '0') {$error[] = 'State is required';}
	if(empty($_POST['zipcode'])) {$error[] = 'Zip code is required';}
//	if($_POST['shortcountry'] == '0') {$error[] = 'Country is required';}
	if(empty($_POST['email'])) {$error[] = 'Email is required';}
	if(empty($_POST['phonenumber'])) {$error[] = 'Phone is required';}
	if(empty($_POST['cardNumber'])) {$error[] = 'Card number is required';}
	if(empty($_POST['CVV'])) {$error[] = 'Security code is required';}
	if($_POST['expirationMonth'] == '-' or $_POST['expirationYear'] == '-') {$error[] = 'Expiry Date is required';}
//	if(!isset($_POST['terms']) or $_POST['terms'] != '1') {$error[] = 'You must agree with terms and conditions';}
	return $error;
}

function SendLead($objLpGeneric,$packageDealID, $testSale) {
	
 	// $client = new nusoap_client('https://api.safe-pay-online.com/WWRemote/trackingV3.asmx?WSDL', true);
	$client = new nusoap_client('https://api.safe-pay-online.com/WellnessSecurreRemote/trackingV3.asmx?WSDL', true);	
	$result = $client->call('SendLeadNewVA', array(
	'username' => 'apiWellnessSecurre', 
	'password' => 'WellnessScr!', 
	'packageDealID' => $packageDealID,  // landing-page
	'campaignID' => $objLpGeneric->caid, 
	'campaignCode' => $objLpGeneric->caCode, 
	'landingpageID' => $objLpGeneric->cid2, 
	'paymentOption' => $_POST['optpaymentterm'], 
	'firstname' => $_POST['firstname'], 
	'lastname' => $_POST['lastname'], 
	'address1' => $_POST['address1'], 
	'city' => $_POST['city'],
	'shortstate' => $_POST['state'], 
	'shortcountry' => 'US', 
	'zipcode' => $_POST['zipcode'], 
	'email' => $_POST['email'], 
	'phonenumber' => $_POST['phonenumber'], 
	'cardNumber' => $_POST['cardNumber'], 
	'CVV' => $_POST['CVV'], 
	'expirationMonth' => $_POST['expirationMonth'], 
	'expirationYear' => $_POST['expirationYear'],
	'misc1' => $objLpGeneric->misc1,
	'TestSale' => $testSale
	));
	
 	if ($client->fault) {
		return false;
	} else {
		$err = $client->getError();
		if ($err) {
			return false;
		} else {
			return $result;
		}
	}
}

function selected($selectMenu, $option) {
	echo ($_POST[$selectMenu] == $option) ? 'selected="selected"' : '';
}

$checkRequiredFields = checkRequiredFields();
if(!$checkRequiredFields) {
	$SendLeadMessage = '';
	$SendLead = SendLead($objLpGeneric, $CONFIG_PACKAGE_ID_OPTS[$_POST['paymenttermopt']], $CONFIG_TEST_SALE);
	
	

	if($SendLead['SendLeadNewVAResult']['success'] == 'true') {
		$_SESSION['custData'] = [
									"customerID" => $SendLead['SendLeadNewVAResult']['customerID'], 
									"billingID" => $SendLead['SendLeadNewVAResult']['billingID'], 
									"processorID" => $SendLead['SendLeadNewVAResult']['processorID']
									];
		$SendLeadMessage = 'send-ok';
	} elseif($SendLead == false) {
		$SendLeadMessage = '<font color="#ff0000" face="Arial" size=1.5em>Error in Web Service Call</font><br>';
	} else {
		$SendLeadMessage = '<font color="#ff0000" face="Arial" size=1.5em>'.$SendLead['SendLeadNewVAResult']['message'].'</font><br>';
		
		
	}
	
	echo $SendLeadMessage;
}
else
{
	foreach($checkRequiredFields as $error=>$value){
		echo $value.".<br />";
	}
}

	$paymentTermOpt = $_POST['paymenttermopt'] ;

	$_SESSION["s_paymentTermOpt"]= $paymentTermOpt;

	$payment_option_term_text=array("$4 per month","$7 per month","$9.95 per month");

	$firstName = $_POST['firstname'];
	$_SESSION["s_firstName"]= $firstName;

	$lastName = $_POST['lastname'];
	$_SESSION["s_lastName"]= $lastName;

	$email = $_POST['email'];
	$_SESSION["s_email"]= $email;

	$phonenumber = $_POST['phonenumber'];
	$_SESSION["s_phonenumber"]= $phonenumber;

	$address1 = $_POST['address1'];
	$_SESSION["s_address1"]= $address1;

	$city = $_POST['city'];
	$_SESSION["s_city"]= $city;

	$zipCode = $_POST['zipcode'];
	$_SESSION["s_zipCode"]= $zipCode;

	$state = $_POST['state'];
	$_SESSION["s_state"]= $state;
	
	$subject = "Billing Information - YogaClub- HealthyFinds";
		
	$content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">'.
				'<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">'.
				'<body style="background-color: #eee; padding-top: 50px; padding-bottom: 50px;">'.
				'<div style="width: 70%; margin: 50px auto 50px auto; background-color: #fff; padding: 20px">'.
				'<h1>'.$subject.'</h1>'.
				'<br />'.
				'<div>'.
				'<p><span style="font-weight: bold;">Payment Option : </span>'.$payment_option_term_text[$paymentTermOpt] .'</p>'.
				'<p><span style="font-weight: bold;">First Name: </span>'.$firstName.'</p>'.
				'<p><span style="font-weight: bold;">Last Name: </span>'.$lastName.'</p>'.
				'<p><span style="font-weight: bold;">Email Address: </span>'.$email.'</p>'.
				'<p><span style="font-weight: bold;">Phone Number: </span>'.$phonenumber.'</p>'.
				'<p><span style="font-weight: bold;">Address: </span>'.$address1.'</p>'.
				'<p><span style="font-weight: bold;">City: </span>'.$city.'</p>'.
				'<p><span style="font-weight: bold;">Zip Code: </span>'.$zipCode.'</p>'.
				'<p><span style="font-weight: bold;">State: </span>'.$state.'</p>'.
				'<p><span style="font-weight: bold;">Flavor: </span>'.$flavor.'</p>'.
				'</div>'.
				'</body>'.
				'</html>';
			   
	$headers = 'Content-type: text/html; charset=iso-8859-1' . "\n" .
					   'From: '. $email . "\n" . 'Reply-To: '. $email . "\n";
	
	 

	$sendTo = "janinetrocks@gmail.com,dan@bcmdirect.net,dan@healthy-finds.com,edeliver@edeliversolutions.net,dan.steece@gmail.com";
//	@mail($sendTo, $subject, $content, $headers)
?>