<?php include 'components/header.php'; ?> 
<section id="order-section"  >
    <div class="container"> 
 
        <!-- Start Content -->
        <div class="row content">
            <div class="col-md-6 col-sm-6 col-xs-12">
            <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
            <img class="img-responsive" src="assets/images/thank-you-product.png" />
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 ">
                <div class="whitespace hidden-xs" style="height:50px;">&nbsp;</div>
                <h1 class="font-chelsea big-font"><span >THANK</span> <span class="purple3">YOU!</span></h1>
                <div class="gray-fade-box">                    
                    <p class="paragraph">

                      Thank you for trying the Healthy Finds Savings Program! With this program, you will save hundreds on health and wellness products as our wellness team scours the web to find the most innovative health products and then negotiates special deals just for you. A receipt has been sent to your email address while your free $40  Model Meals coupon will be emailed in 2 business days. As for your free $40 Healthy-Finds Gift Cards, please expect them in your inbox within the next 10 minutes. Otherwise, please contact us by email at
                       <a href="tel:1-866-949-7949">1-866-949-7949</a> between the hours of 10 am – 8 pm EST.
                </p>	                     
                    <p class="paragraph">You can start earning cash back on every $50 you spend on super healthy brands by clicking <a href="https://www.healthy-finds.com/cashback/" target="_blank">here</a>.</p>	 
                    <p class="paragraph">As an added bonus, you get a free copy of Dr. Andrew Campbell’s groundbreaking book, The Key to Longevity. Just click on the link below:</p>
                    <p class="paragraph"><a href="https://drandrewcampbell.com/ebook/inflamation.pdf">The Key to Longevity</a></p>
                </div>
            </div>
        </div>
    </div> 
</section>
<?php include 'components/footer.php'; ?>