<?php include 'components/header.php'; ?>
<?php include 'LpGeneric.php';
	//init object
	$objLpGeneric = LpGeneric::RestoreObject();
?>
<?php echo $objLpGeneric->ShowPixel(true);?>
<section id="landing-page-section" class="padded-section">

	<!-- Banner -->
	<div class="row header-top" >	
 	
		<div class="col-md-12 col-sm-12 col-xs-12  align-center">
			<h1>First time ever offered!</h1>
			<h4>Instant discount of $45 on your next purchase at Zing Meals when you spend $60 or more. Up to 75% Savings!</h4>
	 	
		</div>
	</div>
	
	<div class="banner no-padding no-margin"  >
		
		<a href="order.php" >
			<img id="banner-image" class="img-responsive" src="assets/images/landing-page-banner.jpg?q1213" usemap="#banner-map" />
		</a>
		 
	</div>

	<!-- Start Content -->
	
	<div class="container"> 
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<img   class="img-responsive" src="assets/images/mid-content.jpg?q1213"  />
			 	
				<h4 class="align-center  s ">Zing Meals’ exclusive offer saving you $45 off the delicious meals of your choice through a new partnership with Health-Finds.</h4>
				<h4 class="align-center  s ">Healthy-Finds is a revolutionary program that saves you money on the healthiest brands online.</h4>

			</div>
		</div>

 

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<br/>
				<p class="paragraph f-reg align-center ">
					When you become a Healthy-Finds member, you’ll get an instant discount to use on ZingMeals.com of <br/> $45 to use like cash when you spend $60 or more.  Up to a 75% discount on the best meals around.  
				</p>
				<p class="paragraph f-reg align-center ">
					You’ll also receive $40 in gift cards from super healthy brands like Drink UVO, Sweetie Pie organic baby food, <br/> Skin 2 Skin Natural Skincare, and dozens more.
				</p>
				<!-- <p class="paragraph align-center">
					That is a savings of $100 plus Free Shipping!

				</p> -->
			</div>
		 
				<div class="col-md-12 col-sm-12 col-xs-12">
					<a href="order.php" class="landing-button align-center  "  >YES! Save $45 on Zing Meals Now!</a>
				</div>
		 
		</div>

		 
	</div>
 
 
		<div class="container"> 
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<br/>
					<h3  ><span class=" s  " >IMAGINE HAVING YOUR OWN TEAM OF EXPERTS CAREFULLY SELECTING THE HEALTHIEST PRODUCTS AND NEGOTIATING SPECIAL DEALS JUST FOR YOU.</h3>
					</br> 
					<h4 class="black  " >Sounds pretty good, huh?</h4>
					<h4 class="black  " >Join Healthy-Finds Today for as little as $4 and receive:</h4>
	
					<ul class="no-list-type ">
						<li><i class="fa fa-gift fa-fw black "></i> $45 Gift code to use instantly at ZingMeals.com (Minimum purchase of $60)</li>
						<li><i class="fa fa-credit-card fa-fw black"></i> $40 in gift cards good at dozens of super healthy brands like Russell Organics, Wild Foods, Nuzest, and Sarah’s Snacks</li>
						<li><i class="fa dollar-sign  fa-faw black">$</i> Hundreds of $$ in health savings and free gifts each month</li>
						<li><i class="fa fa-user-md fa-fw black"></i> A wellness team available to answer all your questions.</li>
					</ul>
					<br/>
		
				
					<h4 class="align-center s">The moment you subscribe, we’ll send you a coupon code for $45 off your next meal delivery from Zing Meals- plus another $40 in free gift cards to other amazing health brands.</h4>
					<br/>
					<br/>
					
					<br/> 
					<br/> 
				
					<br/> 
				</div>
			</div>
			
		</div>
 

	<div class="container"> 
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<a href="order.php" class="landing-button align-center  "  >JOIN HEALTHY-FINDS AND GET $85 IN SAVINGS</a>
				</div>
			</div>
	</div>


	 <br/><br/><br/>
	 <div class="align-center">
		<span id="siteseal"><script async type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=XdUoaf3YDei8wtp6fyFLCJuQSl7tT1MpFM3JD2ottwHStCNQTVCrs6s0SBg0"></script></span>  			
		<script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async></script> 		
		<br/><br/>
	</div>
	
</section>




<?php include 'components/footer.php'; ?>
<!--::::::::::::::::::::::::::::::::::::::::::::::::<?php echo  ':::' . $CONFIG_TEST_SALE;  ?>::::::::::::::::::::::::-->
