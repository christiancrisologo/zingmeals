// Main JavaScript File
$(document).ready(function() {
  var _upSellLoc = 'exclusive-hf.php';
  var _thankYouUpSellLoc = 'thank-you-upsell.php';
  var _thankYouUpSellHFLoc = 'thank-you-hfwgc.php';
  var _thankYouUpSellMWLoc = 'thank-you-dcwp.php';

  $('#billing-information').on('submit', function(event) {
    event.preventDefault();
    event.stopPropagation();
    event.stopImmediatePropagation();

    var formData = $(this).serialize();
    $.ajax({
      url: 'actions/send-billing-form.php',
      type: 'POST',
      data: formData,
      beforeSend: function() {
        var loading =
          "<div class='alert alert-info'>Processing your request... <i class='fa fa-spinner fa-fw fa-spin'></i></div>";
        $('#billing-form-response').html(loading);
      },
      success: function(actionResponse) {
        var response = actionResponse.trim();
        if (response == 'send-ok') {
          document.location = _thankYouUpSellHFLoc;
        } else {
          var error = "<div class='alert alert-danger'>" + response + '</div>';
          $('#billing-form-response').html(error);
        }
      },
    });
  });

  // $('.send-cookbook-button').on('click', function () {
  // 	document.location = "https://www.healthy-finds.com/westlab/order.php";
  // });

  var yesOffer = function(event) {
    event.preventDefault();
    event.stopPropagation();
    event.stopImmediatePropagation();

    $.ajax({
      url: 'actions/process_upsell.php',
      type: 'POST',
      data: 'abc',
      beforeSend: function() {},
      success: function(actionResponse) {
        var response = actionResponse.trim();
        if (response == 'send-ok') {
          document.location = _thankYouUpSellHFLoc;
        } else {
          var error = "<div class='alert alert-danger'>" + response + '</div>';
          $('#billing-form-response').html(error);
          $('#offer').effect('shake', { direction: 'up', distance: 5, times: 4 });
          $('#notation-container').html(
            '<div class="alert alert-danger">You must agree with terms and conditions.</div>'
          );
        }
      },
    });
  };

  $('#yes-offer').on('click', function(event) {
    yesOffer(event);
  });
  $('#yes-offer2').on('click', function(event) {
    yesOffer(event);
  });

  $('#no-offer').on('click', function() {
    if ($('#yes-i-agree').is(':checked')) {
      $('#offer').effect('shake', { direction: 'up', distance: 5, times: 4 });
    } else document.location = _thankYouUpSellLoc;
  });

  $('#yesiwant').on('click', function(event) {
    event.preventDefault();
    event.stopPropagation();
    event.stopImmediatePropagation();

    var formData = $(this).serialize();
    $.ajax({
      url: 'actions/process_specialOffer.php',
      type: 'POST',
      data: formData,
      beforeSend: function() {
        var loading =
          "<div class='alert alert-info'>Processing your request... <i class='fa fa-spinner fa-fw fa-spin'></i></div>";
        $('#billing-form-response').html(loading);
      },
      success: function(actionResponse) {
        var response = actionResponse.trim();
        if (response == 'send-ok') {
          document.location = 'upsell-gifts-mw.php';
        } else {
          var error = "<div class='alert alert-danger'>" + response + '</div>';
          $('#billing-form-response').html(error);
        }
      },
    });
  });

  $('.date-today').html(
    new Date().toLocaleDateString('en-US', {
      day: '2-digit',
      month: 'long',
      year: 'numeric',
      hour12: false,
    })
  );
});
