<?php session_start();?>
<?php require_once(__DIR__ .  '/../config.php'); ?>

<!DOCTYPE html>
<html lang="en">
	<head>
    <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P9JCGGJ');</script>
<!-- End Google Tag Manager -->

		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge"> 
		<title>FREE <?php echo $CONFIG_SITE_TITLE ; ?> Gift Card | <?php echo  isset($CONFIG_SITE_TITLE) ?  $CONFIG_SITE_TITLE .'| ' : ''?> Healthy Finds</title>
		<meta name="title" content="FREE <?php echo $CONFIG_SITE_TITLE ;?> Gift Card | Healthy Finds">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" type="image/ico" href="assets/images/favicon.png"/>		
		
		<!-- Bootstrap core CSS -->
		<link rel="stylesheet" href="assets/bootstrap-3.3.7-dist/css/bootstrap.css" />
		<link rel="stylesheet" href="assets/bootstrap-3.3.7-dist/css/bootstrap-dialog.min.css" />
		<!-- Font Awesome CSS -->
		<link rel="stylesheet" href="assets/font-awesome-4.7.0/css/font-awesome.min.css" />

	<!-- Main Stylesheet -->
		<link rel="stylesheet" href="assets/css/main.css<?php getRand()?>" />
		<!-- Vendors Stylesheet -->
		<link rel="stylesheet" href="assets/css/vendor.css<?php  getRand()?>"  />
		<!-- GOOGLE FONTS -->
		<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900|Oswald:400,500,700|Montserrat:400,700,900|Oswald:400,500,700" rel="stylesheet">

		<!-- Start Alexa Certify Javascript -->
		<script type="text/javascript">
		_atrk_opts = { atrk_acct:"Hyeup1IWh910Y8", domain:"healthy-finds.com",dynamic: true};
		(function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://d31qbv1cthcecs.cloudfront.net/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
		</script>
		<noscript><img src="https://d5nxst8fruw4z.cloudfront.net/atrk.gif?account=Hyeup1IWh910Y8" style="display:none" height="1" width="1" alt="" /></noscript>
		<!-- End Alexa Certify Javascript -->  

        		<!-- Start Visitors and track conversions -->
		<script type="text/javascript">
		(function() {
			window._pa = window._pa || {};
			// _pa.orderId = "myOrderId"; // OPTIONAL: attach unique conversion identifier to conversions
			// _pa.revenue = "19.99"; // OPTIONAL: attach dynamic purchase values to conversions
			// _pa.productId = "myProductId"; // OPTIONAL: Include product ID for use with dynamic ads
			var pa = document.createElement('script'); pa.type = 'text/javascript'; pa.async = true;
			pa.src = ('https:' == document.location.protocol ? 'https:' : 'http:') + "//tag.marinsm.com/serve/5a1ca7c2187073c471000117.js";
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(pa, s);
		})();
		</script>
		<!-- End Visitors and track conversions -->

	</head>
	<body>
    <!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P9JCGGJ" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
