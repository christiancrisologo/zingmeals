		<footer>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
					 	<div class="bottom-footer align-center">  
							&copy; 2018 <a href="https://healthy-finds.com" target="_blank">Healthy-Finds</a></span>
							<br /> Elol Enterprises LLC, 3736 Bee Cave Rd, Suite 1-259 West Lake Hills, TX 78746
							<br />
							<a href="terms.php">Terms and Conditions</a> | <a href="policy.php">Privacy Policy</a> 
						</div>
					</div>
				</div>
			</div>		
		</footer>
		<!-- jQuery script file -->
		<script type="text/javascript" src="assets/jquery/jquery.v2.1.4.min.js"></script>
		<script type="text/javascript" src="assets/jquery/jquery-ui.js"></script>
		<!-- Bootstrap core files -->
		<script type="text/javascript" src="assets/bootstrap-3.3.7-dist/js/bootstrap.js"></script>
		<script type="text/javascript" src="assets/bootstrap-3.3.7-dist/js/bootstrap-dialog.min.js"></script>
		<!-- Main JS -->
		<script type="text/javascript" src="assets/js/main.js<?php getRand()?>"></script>
	</body>
</html>