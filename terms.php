<?php 
	$PAGE_TITLE = " Terms and Condition  ";
	include 'components/header.php'; 
?>
<section id="landing-page-section" class="padded-section">
	<div class="container">
 
		<div class="row">
			<h2>Terms and Conditions</h2>
			<p class="paragraph">These Terms and Conditions of Use (“Terms”) are a legal contract between Elol Enterprises LLC (collectively, “us” or “we”), and you, the user accessing or using this web site.</p>
			<p class="paragraph">PLEASE READ THESE TERMS CAREFULLY BEFORE USING THIS SITE. THESE TERMS DESCRIBE YOUR RIGHTS AND RESPONSIBILITIES IN CONJUNCTION WITH YOUR USAGE OF THE SITE. BY ACCESSING ANY PORTIONS OF THE SITE, YOU AGREE THAT YOU HAVE READ AND UNDERSTOOD THESE TERMS AND AGREE TO BE BOUND BY THESE TERMS JUST AS IF YOU HAD SIGNED THEM. YOU ARE ALSO SUBJECT TO ANY ADDITIONAL TERMS IN ANY INFORMATION, MATERIAL OR SERVICES YOU ACCESS FROM THE SITE. ALL SUCH ADDITIONAL TERMS ARE INCORPORATED BY REFERENCE INTO THESE TERMS. WE MAY MODIFY THESE TERMS AT ANY TIME WITHOUT NOTICE TO YOU, AND SUCH MODIFICATIONS WILL BE EFFECTIVE IMMEDIATELY UPON POSTING OF THE MODIFIED TERMS. PLEASE REVIEW THESE TERMS EACH TIME YOU USE THE SITE.</p>
			<p class="paragraph">By using this Site, you agree to be bound by these Terms, and your continued use of this Site following any modification to the Terms, means you accept and agree to be bound by such modifications. If you do not agree to all of these Terms of use, you are not permitted to use this Site.</p>
			<h3>1. USE OF THE SITE AND CONTENT</h3>
			<p class="paragraph">The contents of the Site such as text, graphics, images, audio, videos, Submissions (as defined below), works of authorship and other material contained on the Site (“Content”) are for informational purposes only. You may view or download a single copy of the Content on the Site solely for your personal, noncommercial use if you retain all copyright and proprietary rights notices that are contained in the Content. Any special rules for the use of certain software and other Content accessible on the Site may be included elsewhere within the Site and are incorporated into these Terms by reference. Hypertext linking to this Site is permitted by you. However, we may disable any such link at our sole discretion. Framing or any other use of this Site that serves to affect the identity of this Site or the source of the Content is prohibited. This first paragraph of Section 1 is collectively “Authorized Use(s).”</p>
			<p class="paragraph">Content and features are subject to change or termination without notice at our sole discretion. All rights not expressly specified in the Authorized Uses are reserved to us and our licensors. If you violate any of these Terms, your permission to use the Site and to access the Content automatically terminates and you must immediately destroy any copies you have made of any portion of the Content.</p>
			<p class="paragraph">You may not use the Site or Content to: (i) perform any activity which is or may be, directly or indirectly, unlawful, harmful, threatening, abusive, harassing, tortuous, defamatory, vulgar, obscene, libelous, invasive of another’s privacy, hateful or racially, ethnically or otherwise objectionable or interfering with the performance of the Site; (ii) perform any activity which will or may breach a third party right; (iii) engage in public or commercial activities, including but not limited to offering for sale any products or services, soliciting for advertisers or sponsors or selling, licensing or granting public access to any information offered on the Site; (iv) copy, publish, broadcast, modify, distribute or transmit the Content not otherwise authorized in these Terms; (v) conduct any other act or omission not expressly authorized or required by these Terms (“Prohibited Use(s)” ). You shall not use the Site to gain any unauthorized access to, or conduct any Prohibited Use on, any other software, equipment, system or network.</p>
			<h3>2. THIRD PARTIES</h3>
			<p class="paragraph">When using the Site, the Content will be transmitted over a medium that may be beyond the control and jurisdiction of us and our affiliates. Accordingly, we assume no liability for or relating to the delay, failure, interruption, or corruption of any Content transmitted in connection with use of the Site.</p>
			<p class="paragraph">We may provide links to third-party web sites (“Third Party Sites”) and may from time to time provide third party material (“Third Party Materials”) on the Site. We also may select certain sites as priority responses to search terms you enter and we may agree to allow advertisers to respond to certain search terms with advertisements or sponsored content. These Third Party Sites and Third Party Materials are only provided as a convenience to you. The inclusion of any Third Party Materials or links to Third Party Sites is not and does not imply an affiliation, sponsorship, endorsement, approval, investigation, verification or monitoring by us of any such information or site. The terms and conditions of such sites are likely to be different from these Terms. We do not recommend and do not endorse the Third Party Sites or Third Party Materials. We are not responsible for Third Party Sites or Third Party Materials, sites framed within the Site, third-party sites provided as search results, or third-party advertisements, and does not make any representations regarding their content or accuracy. Your use of third-party websites is at your own risk and subject to the terms and conditions of use for such sites. We do not endorse any product advertised on the Site.</p>
			<h3>3. ADULT AND TEEN USAGE</h3>
			<p class="paragraph">The Site shall be used only by persons who are at least 13 years old. If you are younger than 13 years old, you may not access the Site. If you are a parent or other person charged with the care of persons younger than 13 years old, you agree to restrict access to the Site through your computers to persons who are at least 13 years old. If one of your children accesses the Site and provides us with personally identifiable information, promptly contact us in writing whereby we will use reasonable efforts to delete all personally identifiable information regarding such minor from its databases.</p>
			<h3>4. DISCLAIMERS AND WARNINGS</h3>
			<p class="paragraph">
				<strong>4.1 THE SITE MAKES NO GUARANTEE OF VALIDITY</strong><br />
				Be advised that although the Content has been written and/or reviewed by healthcare professionals, the Content may not be completely accurate. Although you may find valuable and accurate Content on the Site, we do not guarantee the accuracy or validity of the Content, and the Content is not peer reviewed. If you need specific medical advice, visit your healthcare professional.
			</p>
			<p class="paragraph">Access to or viewing of certain Content could be in violation of the laws of the country or jurisdiction from where you are viewing such information. The Site is operated on servers in the United States of America, and is maintained under the protections of local, state and federal law. Laws in your country or jurisdiction may not protect or allow the same kinds of publication, downloading, speech or distribution. We do not encourage the violation of any laws; and is not responsible for any violations of such laws arising from your use of the Site.</p>
			<p class="paragraph">We make no representation or warranty as to the satisfaction of any government regulations requiring disclosure of information on prescription drug products or the approval or compliance of any software tools with regard to the Content contained on the Site.</p>
			<p class="paragraph">
				<strong>4.2 THE SITE DOES NOT PROVIDE MEDICAL ADVICE</strong><br />
				The contents of our Site, such as text, graphics, images, information obtained from our licensors, and other material contained on the Site (“Content”) are for informational purposes only. The Content is not intended to be a substitute for professional medical advice, diagnosis, or treatment. Always seek the advice of your physician or other qualified health provider with any questions you may have regarding a medical condition. Never disregard professional medical advice or delay in seeking it because of something you have read on our Site!
			</p>
			<p class="paragraph">If you think you may have a medical emergency, call your Health Mentor or 911 immediately.</p>
			<p class="paragraph">We do not recommend or endorse any specific tests, physicians, products, procedures, opinions, or other information that may be mentioned on the Site. Reliance on any information provided by us, our employees, others appearing on the Site at our invitation, or other visitors to the Site is solely at your own risk.</p>
			<p class="paragraph">The Site may contain health- or medical-related materials that are sexually explicit. If you find these materials offensive, you may not want to use our Site.</p>
			<p class="paragraph">
				<strong>4.3 USE THE SITE AT YOUR OWN RISK</strong><br />
				Any reliance or use of the Site or the Content is solely at your own risk.
			</p>
			<p class="paragraph">INFORMATION PROVIDED THROUGH THIS SERVICE MAY NOT BE APPROPRIATE FOR YOU.</p>
			<p class="paragraph">You should take all steps necessary for you to ascertain that any Content is correct. Check all references in each article and double-check all Content with independent sources.</p>
			<p class="paragraph">The Site is not sufficient to inform you about adequate safety precautions.</p>
			<p class="paragraph">
				<strong>4.4 DISCLAIMER OF WARRANTIES</strong><br />
				The Site and Content are provided on an “as is” basis. WE, OUR LICENSORS, AND SUPPLIERS, TO THE FULLEST EXTENT PERMITTED BY LAW, DISCLAIM ALL REPRESENTATIONS AND WARRANTIES REGARDING THE SITE AND THE CONTENT, EITHER EXPRESS OR IMPLIED, STATUTORY OR OTHERWISE, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, NON-INFRINGEMENT OF THIRD PARTIES’ RIGHTS, AND FITNESS FOR PARTICULAR PURPOSE. YOU ACKNOWLEDGE AND AGREE THAT ANY AND ALL DISCLAIMERS IN THESE TERMS AND THE PROVISIONS OF THIS SECTION REFLECT A FAIR AND REASONABLE ALLOCATION OF RISK BETWEEN US AND YOU.
			</p>
			<p class="paragraph">Without limiting the foregoing, we, our licensors, and its suppliers make no representations or warranties about the following:</p>
			<ul>
				<li><p class="paragraph">The accuracy, reliability, completeness, currentness, or timeliness of the Content or communications provided on or through the use of the Site.</p></li>
				<li><p class="paragraph">The satisfaction of any government regulations requiring disclosure of information on prescription drug products or the approval or compliance of any software tools with regard to the Content contained on the Site.</p></li>
			</ul>

			<p class="paragraph">
				<strong>4.5 TERM OF SALE</strong><br /> 
				I am aware that the offers on this site are for non-members and that if I choose to avail of any offer, I have to pay the total price quoted therein.  Furthermore, I know that I also have the option to upgrade my purchase by signing up to be a member of the Healthy Finds Savings Program where I can benefit from additional discounts and freebies.  I know that membership to the Healthy Finds Savings Program is only $7 per month, which will automatically be debited from my credit card every 30 days for my payment convenience.  I understand that if I do not wish to continue with the membership, I only have to give due notice of cancellation via phone or e-mail within the first 30 days or current month so that I will not be charged in the succeeding month/s.     
			</p>
			<p class="paragraph">
				I also am aware that <a href="https://healthy-finds.com/" target="_blank">healthy-finds.com</a> will only share customer’s name, address, and email address with the product company. No other information will be passed along to the product company. The product company may send out emails such as newsletters. If I do not wish to receive them, I will simply follow the unsubscribe directions found in the email. No information other than name, address and email address will be shared.
			</p>

		 	<p class="paragraph">
				<strong>4.6 REFUND POLICY</strong><br />
				We offer a 100% money back guarantee if customer is not happy with purchase within 30 days. Simply call 1-866-949-7949 between 10:00am – 8:00pm Eastern, Monday through Friday.
			</p>
			<h3>5. LIMITATION OF LIABILITY</h3>
			<p class="paragraph">In no event shall we or our contributors, administrators, officers, directors, members, managers, agents, employees, licensors, suppliers or any third parties mentioned on the Site be liable for any damages (including, without limitation, direct, indirect, special, punitive, incidental and consequential damages, personal injury/wrongful death, lost profits, or damages resulting from lost data or business interruption) arising out of or relating to the Site and Content or resulting from the use of or inability to use the Site or the Content, whether based on warranty, contract, tort, strict liability, or any other legal theory, and whether or not we are advised of the possibility of such damages. To the extent that the foregoing limitations are found to be invalid in any way, the maximum aggregate liability to you of the Parties shall not exceed the fees (if any) paid by you for the particular Content or portion of the Site that is the subject of the claim. We are not liable for any personal injury, including death, caused by your use or misuse of the Site or Content. Any claims arising in connection with your use of the Site or any Content must be brought within one (1) year of the date of the event giving rise to such action occurred. You hereby waive and release us from any and all claims arising in connection with the Site or the Content that are not brought within such period. Remedies under these Terms are exclusive and are limited to those expressly provided for in these Terms.</p>
			<h3></h3>
			<p class="paragraph"></p>
			<h3>6. INDEMNITY</h3>
			<p class="paragraph">You agree to defend, indemnify, and hold us harmless from and against any claims, actions or demands, liabilities and settlements including without limitation, all damages, judgments, fines, costs, and expenses (including reasonable legal and accounting fees), resulting from, or alleged to result from, (i) your violation of these Terms, (ii) your access of the Site, (iii) from your use of the Content, or (iv) any Submissions or Suggestions (each as defined below) submitted by you.</p>
			<h3></h3>
			<p class="paragraph"></p>
			<h3>7. INTELLECTUAL PROPERTY</h3>
			<p class="paragraph">
				<strong>7.1 GENERAL</strong><br />
				I understand I will pay the price quoted for the product above, and after the purchase I have the choice of accepting 2 free gifts from Dr. Andrew Campbell. If I accept, I have 30 days to cancel the wellness program being offered. If I do not cancel, I will be charged $7.00 every 30 days. That will allow me to continue using the Savings Program up until I cancel. I also am aware that healthy-finds.com will only share customers name, address, and email address with the product company. No other information will be passed along to the product company. The product company may send out emails such as newsletters. If you do not wish to receive them, simply follow the unsubscribe directions found in the email. No information other than name, address and email address will be shared.
			</p>
			<p class="paragraph">
				<strong>7.2 COPYRIGHTS</strong><br />
				All Content and other works on the Site and any reproductions of any of the foregoing are the copyrighted works of us and/or our affiliates or licensors, and are protected under U.S. and worldwide laws and treaty provisions. Other than the limited grant of access to, viewing, printing and saving of the Content as set forth above, we grant you no other privileges or rights in any of the Content. You acknowledge that any other use of the Site or Content, including without limitation, framing, reproduction, modification, distribution, transmission, republication, display or performance, without the prior written permission of us and/or our affiliates or licensors, is strictly prohibited.
			</p>
			<p class="paragraph">
				<strong>7.3 TRADEMARKS</strong><br />
				All page headers, custom graphics, logos, button icons and other distinctive words, names, signs, symbols, expressions, sounds, images, colors, themes and packaging on the Site are the trademarks, service marks, trade dress, collective marks, design rights, personality rights or similar rights (“Marks”) the property of their respective owners. Their use does not imply that you may use them for any other purpose other than for the same or a similar informational use as contemplated by these Terms. Your use of any such or similar incorporeal property is at your own risk.
			</p>
			<p class="paragraph">You shall not display, use or reproduce any of the Marks in any manner without prior written consent from us. Without limiting the generality of the foregoing, we specifically prohibit the use of any Mark in conjunction with a link to any web site unless we approve the establishment of such a link in advance in writing. Failure to obtain such approval shall entitle us to seek an injunction enjoining such use.</p>
			<h3>8. POLICY</h3>
			<p class="paragraph">Any personal information that you submit to us is governed by our Privacy Policy. To the extent there is an inconsistency between this Agreement and the Privacy Policy, this Agreement shall govern.</p>
			<h3>9. ENHANCEMENT SUGGESTIONS</h3>
			<p class="paragraph">If you have any suggestions, ideas, concepts, improvements, or other enhancements with respect to the Site (collectively, “Suggestions”) that you submit via email or otherwise through the Site, such Suggestions are non-confidential for all purposes and shall be deemed a grant of permission by you for us to adopt and use such Suggestions without additional consideration. If you wish to keep any Suggestions private or proprietary, do not submit them to this Site via email or otherwise through the Site. To keep any such Suggestions private or proprietary to you, you must separately contact us, and if we desire to do so, enter into appropriate written agreements with us regarding such Suggestions.</p>
			<h3>10. NOTICE AND TAKEDOWN PROCEDURES</h3>
			<p class="paragraph">If you believe any materials accessible on or from the Site infringe your copyright, you may request removal of those materials (or access thereto) from this web site by providing us with the following information at the address below:</p>
			<p class="paragraph">Identification of the copyrighted work that you believe to be infringed. Please describe the work, and where possible include a copy or the location (e.g., URL) of an authorized version of the work.</p>
			<p class="paragraph">Identification of the material that you believe to be infringing and its location. Please describe the material, and provide us with its URL or any other pertinent information that will allow us to locate the material.</p>
			<p class="paragraph">Your name, address, telephone number and (if available) e-mail address.</p>
			<p class="paragraph">A statement that you have a good faith belief that the complained of use of the materials is not authorized by the copyright owner, its agent, or the law.</p>
			<p class="paragraph">A statement that the information that you have supplied is accurate, and indicating that under penalty of perjury, you are the copyright owner or are authorized to act on the copyright owner’s behalf.</p>
			<p class="paragraph">A signature or the electronic equivalent from the copyright holder or authorized representative.</p>
			<br />
		</div>
	</div>
	<?php include 'components/seals.php'; ?>
</section>
<?php include 'components/footer.php'; ?>